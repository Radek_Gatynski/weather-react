import React from "react";
import './TitleApp.css';
class TitleApp extends React.Component {
 render(){
     return (
         <div>
            <header>
                <h1>Weather Finder</h1>
                <p>Find out temperture, conditions and more...</p>
            </header>        
         </div>
     );
 }
}

export default TitleApp;