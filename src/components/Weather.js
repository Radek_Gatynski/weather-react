import React from "react";
import './Weather.css';
class Weather extends React.Component{
    render(){
        return (
            <div>
                <div className="results">
                    {this.props.city && this.props.country && <p>Location: {this.props.city}, {this.props.country}</p>}
                    {this.props.temp && <p>Temperature: {this.props.temp} &ordm;C</p>}
                    {this.props.humidity && <p>Humidity: {this.props.humidity}%</p>}
                    {this.props.description && <p>Description: {this.props.description}</p>}
                    {this.props.error && <p>{this.props.error}</p>}
                </div>
            </div>
          
        );
    }
}
export default Weather;