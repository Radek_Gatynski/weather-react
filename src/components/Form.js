import React from 'react';
import './Form.css';
class Form extends React.Component{
    render (){
        return (
            <div>
                <form onSubmit={this.props.getWeather} className="formCSS">
                <div className="form-group">
                    <label htmlFor="cityName">City name</label>
                    <input id="cityName" type="text" name="city" placeholder="enter city name..." className="form-control" ></input>
                    
                </div>
                <div className="form-group">
                     <label htmlFor="countryCode">Country code</label>
                     <input id="countryCode"  type="text" name="country" placeholder="enter country code..."  className="form-control"></input>
                     <small className="form-text text-muted">To get informations click button</small>
                </div>         
                    <button className="btn btn-primary">Show weather</button>
                </form>
                </div>
           
        );
    }
}
export default Form;