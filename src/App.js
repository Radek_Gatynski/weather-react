import React from "react";

import TitleApp from "./components/TitleApp";
import Form from './components/Form';
import Weather from './components/Weather';

const apiKEY  = "30b55ebfccc03fbc1dcaeeb720e04b23";

class App extends React.Component{
  state = {
    temp: undefined,
    city: undefined,
    country: undefined,
    humidity: undefined,
    description: undefined,
    error: undefined
  }
  getWeather = async (e) => {
    e.preventDefault();
    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;
    const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${apiKEY}&units=metric`);
    const data = await api_call.json();
    if(city && country){
      this.setState({
        temp: data.main.temp,
        city: data.name,
        country: data.sys.country,
        humidity: data.main.humidity,
        description: data.weather[0].description,
        error: ""
      })
    }
    else{
      this.setState({
        temp: undefined,
        city: undefined,
        country: undefined,
        humidity: undefined,
        description: undefined,
        error: "Please enter the value"
      })
    }

  }
  render(){
    return (
      <div>
        <TitleApp />
        <div className="container">
              <div className="row">
                  <div className="col">
                     <Form getWeather={this.getWeather} />
                  </div>
                  <div className="col">
                    <Weather 
                      temp={this.state.temp} 
                      city={this.state.city}
                      country={this.state.country}
                      humidity={this.state.humidity}
                      description={this.state.description}  
                      error={this.state.error} 
                    />
                  </div>
              </div>
        </div>
        
      </div>

    );
  }
};

export default App;